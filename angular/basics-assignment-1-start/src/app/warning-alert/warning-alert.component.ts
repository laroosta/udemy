import { Component } from '@angular/core';

@Component({
    selector: 'app-warning-alert',
    template: `
        <h3>This is Warning Alert!</h3>
    `,
    styles: [`
        h3 {
            color: gold;
        }
    `]
})
export class WarningAlertComponent { }
